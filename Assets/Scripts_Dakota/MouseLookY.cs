﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLookY : MonoBehaviour
{
    //variables
    [SerializeField]
    private float _sensitivity = 1.0f;
    private float rotX;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        LookY();
	}

    void LookY()
    {
        float _mouseY = Input.GetAxis("Mouse Y");
        Vector3 newRotation = transform.localEulerAngles;
        newRotation.x -= _mouseY * _sensitivity;

        rotX -= _mouseY;
        rotX = Mathf.Clamp(rotX, -90, 90);

        transform.localEulerAngles = new Vector3(rotX, transform.localEulerAngles.y, transform.localEulerAngles.z);

    }
}
